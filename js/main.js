//Форма выбора отелей
var wrapper = $("<div>") //создаем див
  .insertBefore(".modal-search-form-without-shadow")
  .click(closePopup)
  .css({
    display: "none",
    position: "fixed",
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,.2)",
    top: 0,
    left: 0,
    zIndex: 4
  });

$(window).on("keydown", function(event){
  if (event.keyCode == 27) closePopup();
});

var currentPopup;

function openPopup(popup) {
  currentPopup = popup;
  currentPopup.show()
    .closest(".modal-search-form-without-shadow")
    .addClass("modal-search-form-with-shadow");

  wrapper.show();
};

function closePopup() {
  if(!currentPopup) return;

  currentPopup.hide()
    .closest(".modal-search-form-without-shadow")
    .removeClass("modal-search-form-with-shadow");

  wrapper.hide();
  currentPopup = null;
};

var searchPopup = $(".modal-search-form");

if(searchPopup.length) {
  $(".search-btn").click(function(event) {
    event.preventDefault();
    openPopup(searchPopup);
  });

  $(".secondary-btn").click(function(event) {
    event.preventDefault();
    closePopup();
  });
}


//Календарь
$.datetimepicker.setLocale("ru");

$(".date-input").each(function() {
  var calendar = $(this).find(".calendar");
  calendar.datetimepicker({
    timepicker: false,
    format: "d.m.Y",
    minDate: new Date()
  });
  $(this).click(function() {
    calendar.datetimepicker("show");
  });
});


//Кнопки + -
$(".numeric-input").each(function() {

  var input = $(this).find("input[type=text]");
  var counter = input.val();

  $(this).find(".btn-minus").click(function() {
    if (counter > 0) input.val(--counter);
  });

  $(this).find(".btn-plus").click(function() {
    input.val(++counter);
  });
});
